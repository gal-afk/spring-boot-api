package galih.ade.springbootapi.model

import java.util.*
import javax.persistence.Column
import javax.persistence.Id

data class ProductResponse (

    val id: String,

    val name: String,

    val price: Long,

    val quantity: Int,

    val created_at: Date?,

    val updated_at: Date?,

    val deleted_at: Date?
)