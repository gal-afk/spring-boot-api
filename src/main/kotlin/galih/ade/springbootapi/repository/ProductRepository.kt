package galih.ade.springbootapi.repository

import galih.ade.springbootapi.entity.Product
import org.springframework.data.jpa.repository.JpaRepository

interface ProductRepository : JpaRepository<Product, String> {

}