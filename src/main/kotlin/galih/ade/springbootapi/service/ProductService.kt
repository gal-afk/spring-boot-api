package galih.ade.springbootapi.service

import galih.ade.springbootapi.model.CreateProductRequest
import galih.ade.springbootapi.model.ProductResponse

interface ProductService {

    fun create(createProductRequest: CreateProductRequest): ProductResponse

}