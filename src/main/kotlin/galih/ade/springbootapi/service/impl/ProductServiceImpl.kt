package galih.ade.springbootapi.service.impl

import galih.ade.springbootapi.entity.Product
import galih.ade.springbootapi.model.CreateProductRequest
import galih.ade.springbootapi.model.ProductResponse
import galih.ade.springbootapi.repository.ProductRepository
import galih.ade.springbootapi.service.ProductService
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProductServiceImpl(val productRepository: ProductRepository) : ProductService {


    override fun create(createProductRequest: CreateProductRequest): ProductResponse {
        val product = Product (
            id = createProductRequest.id,
            name = createProductRequest.name,
            price = createProductRequest.price,
            quantity = createProductRequest.quantity,
            created_at = Date(),
            updated_at = null,
            deleted_at = null
        )

        productRepository.save(product)

        return ProductResponse(
            id = product.id,
            name = product.name,
            price = product.price,
            quantity = product.quantity,
            created_at = Date(),
            updated_at = null,
            deleted_at = null
        )
    }
}